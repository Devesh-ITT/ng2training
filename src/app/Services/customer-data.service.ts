import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Customer } from '../Models/Customer';
import { AppService } from './app.service';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class CustomerDataService {
    userData = [];

    constructor(private appService: AppService) {
    }

    getAllData(): Observable<any> {
        return this.appService.getAction('assets/Mock/employee.json', true);
    }

    getDataById(id): Customer {
        let requiredUserDetails: Customer;
        this.userData.map(userDetails => {
            if (userDetails.cusId === id) {
                requiredUserDetails = userDetails;
            }
        });
        return requiredUserDetails;
    }

    updateUserInfo(updatedUserInfo: Customer) {
        this.userData.map(
            userDetails => {
                if (userDetails.cusId === updatedUserInfo.cusId) {
                    userDetails = updatedUserInfo;
                }
            }
        );
    }

    deleteUserData(customerId: number) {
        this.appService.deleteAction(customerId, 'assets/Mock/employee.json', true).subscribe(
            x => {
                console.log(x);
            }
        );
    }
}
