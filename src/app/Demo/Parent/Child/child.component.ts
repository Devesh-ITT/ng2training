import { Component, Input } from '@angular/core';

@Component({
 selector: 'app-child',
 templateUrl: 'child.component.html'
})
export class ChildComponent {
    @Input() listDetails ;
    constructor() {
        console.log(this.listDetails);
    }
    alertMe() {
        alert(this.listDetails.name);
        console.log(this.listDetails);
    }
}
