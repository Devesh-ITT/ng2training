import { Component } from '@angular/core';

@Component({
    selector: 'app-parent',
    templateUrl: 'parent.component.html'
})
export class ParentComponent {
    list =  [
        {
            name: 'Devesh',
            age: '21'
        },
        {
            name: 'Ballani',
            age: '20'
        }
    ]
}
