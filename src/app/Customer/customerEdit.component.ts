import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerDataService } from '../Services/customer-data.service';
import { Customer } from '../Models/Customer';

@Component({
    selector: 'edit',
    templateUrl: 'customerEdit.component.html', 
})
export class EditCustomerComponent {
    selectedUserForEdit:Customer;
    userId: number;
    constructor(private customerDetails: CustomerDataService,
        private activatedRouter: ActivatedRoute,
        private router: Router
    ) {
        this.userId = +this.activatedRouter.snapshot.paramMap.get('id');
        this.selectedUserForEdit = this.customerDetails.getDataById(this.userId);
        console.log(this.selectedUserForEdit);
    }

    updateData(type: string, value: string) {
        if (type === 'gender') {
            this.selectedUserForEdit.gender = value;
            console.log(this.selectedUserForEdit);
        }
    }

    editUserInfo() {
        this.customerDetails.updateUserInfo(this.selectedUserForEdit);
        this.router.navigate(['app-customer']);
    }
}
