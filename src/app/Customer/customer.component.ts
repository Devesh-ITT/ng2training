import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerDataService } from '../Services/customer-data.service';
import { AppService } from '../Services/app.service';

import { Customer } from '../Models/Customer';

@Component({
    selector: 'app-customer',
    templateUrl: 'customer.component.html',
})
export class CustomerListComponent {
    userDetailsArray: Customer[] = [];
    customerToEdit: Customer;
    constructor(private customerDetails: CustomerDataService, private router: Router,
    private appService: AppService
    ) {
        this.customerDetails.getAllData().subscribe(
            (data) => {
                this.userDetailsArray = data;
            }
        );
    }

    goToEditCustomer(customerId) {
        this.router.navigate(['edit/' + customerId]);
    }

    deleteUser(cusId) {
        this.customerDetails.deleteUserData(cusId);
    }
}
