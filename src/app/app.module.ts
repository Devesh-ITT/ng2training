import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CustomerListComponent } from './Customer/customer.component';
import { EditCustomerComponent } from './Customer/customerEdit.component';
import { FormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { CustomerDataService } from './Services/customer-data.service';
import { Customer } from './Models/Customer';
import { Routing } from './app.routing';
import { AppService } from './Services/app.service';
import { ParentComponent } from './Demo/Parent/parent.component';
import { ChildComponent } from './Demo/Parent/Child/child.component';

@NgModule({
  declarations: [
    AppComponent, CustomerListComponent, EditCustomerComponent, ParentComponent, ChildComponent
  ],
  imports: [
    BrowserModule, Routing, FormsModule, HttpModule
  ],
  providers: [CustomerDataService, Customer, AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
