export class Customer {
    public cusId: number;
    public cusName: string;
    public DOB: string;
    public email: string;
    public gender: string;
    public address: string;
}