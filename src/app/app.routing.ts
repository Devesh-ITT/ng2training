import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerListComponent } from './Customer/customer.component';
import { EditCustomerComponent } from './Customer/customerEdit.component';

const appRoutes: Routes =
    [
        { path: '', pathMatch: 'full', redirectTo: 'app-customer' },
        { path: 'app-customer', component: CustomerListComponent },
        { path: 'edit/:id', component: EditCustomerComponent },
    ];

export const Routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
